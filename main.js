let usersUrl = 'https://ajax.test-danit.com/api/json/users';
let postsUrl = 'https://ajax.test-danit.com/api/json/posts';

class Card {
    constructor(post, user){
        this.post = post;
        this.user = user;
        // Для кожного екземпляра картки викликаємо функцію createCard()
        // що створює картку в DOM дереві та повертає елемент div документа html.
        // Записуємо у властивість element результат виклику функції, щоб потім мати змогу
        // видалити цей елемент з DOM
        this.element = this.createCard();
        // Для кожного екземпляра картки викликаємо метод addDeleteListener()
        // щоб повісити на хрестик подію при кліку
        this.addDeleteListener();
    }

    createCard(){
        // Створюємо картку в DOM дереві
        let userPost = document.createElement('div');
        userPost.classList.add('user-post');

        let avatar = document.createElement('div');
        avatar.classList.add('avatar');
        avatar.innerHTML = `<img src="./img/no-image.png" alt="no-image" class="avatar-img">`;

        userPost.append(avatar);

        let postInfo = document.createElement('div');
        postInfo.classList.add('post-info');

        let username = document.createElement('div');
        username.classList.add('username');

        let name = document.createElement('span');
        name.classList.add('name');

        let email = document.createElement('span');
        email.classList.add('email');

        let cross = document.createElement('span');
        cross.classList.add('cross-icon');
        cross.innerHTML = '&#10006;';

        postInfo.append(username);

        username.append(name);
        username.append(email);
        username.append(cross);

        let postTitle = document.createElement('div');
        postTitle.classList.add('post-title');

        let postText = document.createElement('div');
        postText.classList.add('post-text');

        postInfo.append(postTitle);
        postInfo.append(postText);

        name.textContent = this.user.name;
        email.textContent = this.user.email;
        postTitle.textContent = this.post.title;
        postText.textContent = this.post.body;

        userPost.append(postInfo);

        // Функція повертає контейнер HTML, що відповідає за відображення картки 
        return userPost;
    }

    addDeleteListener() {
        // Шукаємо для кожної новоствореної картки її особистий хрестик
        const deleteButton = this.element.querySelector('.cross-icon');
        console.log(deleteButton);

        // Для кожного знайденого хрестики навішуємо подію при кліку
        // що видаляє пост з сервера за допомогою фетч запиту з методом DELETE та 
        // видаляє саму картку з DOM дерева за допомогою джаваскріпт
        deleteButton.addEventListener('click', () => {
            fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
                method: 'DELETE'
            })
            .then(response => {
                if (response.ok){
                    this.element.remove();
                }
                else {
                    throw new Error('There is an error with deleting this post!')
                }
            })
            .catch(error => {
                console.log(error.name + ': ' + error.message);
            })
        });
}}

function showNews(users, posts) {
    // Для кожного поста знаходимо користувача, що написав цей пост
    // за допомогою метода масиву find. 
    // id користувача та userId поста повинні співпадати
    posts.forEach(post => {
    const user = users.find(user => user.id === post.userId);

    // Якщо користувач, що написав пост знайшовся,
    // створюємо екземпляр класу Card, передаючи у конструктор 
    // відповідний об'єкт, що містить інформацію про пост та об'єкт, що
    // містить дані про користувача, що написав цей пост
    if (user) {
        const card = new Card(post, user);
        // console.log(card);
        document.body.append(card.element);
    }
    });
}

function fetchUsersAndPosts(){
    // Надсилаємо запити до сервера за допомогою fetch API.
    // Після того, як з сервера завантажилися дані і юзерів, і постів,
    // записуємо ці дані відповідно в змінні users та posts,
    // після чого викликаємо функцію showNews(users, posts), що створює нові екземпляри
    // картки для кожного поста та додає їх до документу
    Promise.all([
        fetch(usersUrl).then(response => response.json()), fetch(postsUrl).then(response => response.json())
    ])
    .then(data => {
        users = data[0];
        posts = data[1];
        console.log(users);
        console.log(posts);
        showNews(users, posts);
    })
    .catch((error) => {
        console.log(error.name + ': ' + 'There is an error with fetching users and posts');
    })
}

fetchUsersAndPosts()

